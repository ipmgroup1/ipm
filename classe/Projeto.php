 <?php


include_once "Databases.php";

class Projeto {
	
	private $id_orientador;
	private $id_coorientador;
	private $id_turma;
	private $nome_projeto;
   
	function cadastraProjeto($id_orientador, $id_coorientador,$id_turma, $nome_projeto){        
		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO `projeto` (`id_orientador` , `id_coorientador` , `cod_turma` , `nome_projeto`) VALUES ($id_orientador,$id_coorientador,$id_turma,'$nome_projeto') ";
		$conexao->exec($consulta);
		return true;
	}

	function retornaNomeProjeto($id_projeto){
		$conexao = Databases:: getConnection();
		$projeto = [];	
		$pesquisa = $conexao->query("SELECT * FROM projeto where id_projeto=$id_projeto");
		$projeto = $pesquisa->fetchAll(PDO::FETCH_ASSOC);
		return $projeto;		
	}

	function pesquisaProjetos(){
		$conexao = Databases:: getConnection();
		$projeto = [];
		$pesquisa = $conexao->query("SELECT * FROM projeto where id_projeto not in (select id_projeto from grupo)");
		
		$projeto = $pesquisa->fetchAll(PDO::FETCH_ASSOC);
		return $projeto;
		}

	function apresentaProjeto($id_turma){
		$conexao = Databases:: getConnection();
		$consulta = 'SELECT * FROM projeto,turma where id_turma ='.$id_turma.' and id_turma = cod_turma;';
		$consulta = $conexao->query($consulta);
		$projeto = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $projeto;
	}	

	function apresentaProjetoAluno($id_aluno){
		$conexao = Databases:: getConnection();
		$consulta = 'SELECT projeto.* from grupo, projeto WHERE grupo.id_projeto=projeto.id_projeto AND (id_aluno1 = '. $id_aluno .' or id_aluno2 = '. $id_aluno .' or id_aluno3 ='. $id_aluno .')';
		$consulta = $conexao->query($consulta);
		$projeto = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $projeto;
	}	

	function apresentaProjetoOrientado($id_usuario){
		$conexao = Databases:: getConnection();
		$consulta = 'SELECT projeto.* from projeto
					 WHERE id_orientador = '.$id_usuario.'
					 	OR id_coorientador = '.$id_usuario;
		$consulta = $conexao->query($consulta);
		$projeto = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $projeto;
	}	
   }