<?php

include_once "Databases.php";

class Usuario {
	
	private $nome;
	private $email;
	private $senha;
	private $id_tipo;
	
	public function __construct($nome, $email,$senha,$id_tipo) {
		$this->nome = $nome;
		$this->email = $email;
		$this->senha = md5($senha);
		$this->id_tipo = $id_tipo;
	} 

	function conferiUsuario(){
		$conexao = Databases::getConnection();
		$consulta = $conexao->query("SELECT*FROM usuario WHERE email = '$this->email'");
		$resultado = $consulta->fetch(PDO::FETCH_OBJ);

		if(isset($consulta) AND !empty($consulta)){
			return true;
		} else {
			return false;
		   
		}
	}

	function cadastraUsuario(){        
		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO usuario ( nome, email, senha, id_tipo) values ('$this->nome','$this->email','$this->senha','$this->id_tipo')";
		$conexao->exec($consulta);
		return true;
	}

	function consultaOrientador(){
		$conexao = Databases:: getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=3");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
		}

	function consultaCoorientador(){
		$conexao = Databases:: getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=4");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
		}

	public static function pesquisaAluno(){
		$conexao = Databases::getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=5");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
	}

	public static function pesquisaAlunoSemProjetos(){
		$conexao = Databases::getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=5 AND 
			id_usuario not in (SELECT id_aluno1 FROM Grupo ) 
			AND id_usuario not in (SELECT id_aluno2 FROM Grupo ) 
			AND id_usuario not in (SELECT id_aluno3 FROM Grupo )");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
	}

   function excluiAluno ($id_aluno){
		$conexao = Databases::getConnection();
		$consulta = "SELECT excluiAluno($id_aluno)";
		$update = $conexao->query($consulta);
		return $update;
	}

	function apresentaAlunos($id_turma){
	 	$conexao = Databases:: getConnection();
	 	$usuario = [];
	 	$consulta = $conexao->query("SELECT * FROM usuario where id_turma = $id_turma and id_tipo = 5" );
	 	$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
	 	return $usuario;
	 	}
}
