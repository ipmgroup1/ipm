<?php

class Databases{
    public static function getConnection(){
        try{
            $conexao= new PDO("mysql:host=localhost;dbname=prfc",'root','');
            $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conexao;
        }   catch (PDOException$erro){
            echo 'Conexão falhou: '. $erro->getMessage();
        }
    }
}