<!DOCTYPE html>
<html lang="en">

<head>

    <title>PRFC</title>

    <meta charset="utf-8">

    <!-- FONTS -->
    <link href="../fontes/font_awesome.css" rel="stylesheet">
    <link href="../fontes/lora.css" rel="stylesheet" type="text/css">
    <link href="../fontes/montserrat.css" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link href="bootstrap-3.3.6/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet"> 

</head>

<body>

    <article class="intro">
        <section class="intro-body">
            <section class="container">
                <section class="row">
                    <section class="col-md-8 col-md-offset-2">
                        
                        <h1 class="brand-heading">IPM</h1>
                        <p class="intro-text">Integrator project manager</p>
                        <p class="intro-text">IFC - Campus Araquari</p>
                        
                        <a href="#login" class="btn btn-primary btn-lg page-scroll">Login</a>
                    </section>
                </section>
            </section>
        </section>
    </article>
    
    <section id="login" class=" login container content-section text-center">
        <section class="container">
        <section class="row">
            <section class="form-dialog ">
               <section class="col-md-6 col-md-offset-3">
                <section class="form-top">
                    <h3>Login</h3>
                </section>
                                <form class="signup-form " method="post" action="../controladores/controlador_login.php" role="form">
                            <section class="form-group">
                                <section class="form-input">
                                    <input class="form-control" data-validation="senha" data-validation-error-msg="Por favor preencha o campo!" name="email" placeholder="Email" type="email">
                                </section>  
                            </section>
                            <section class="form-group">
                                <section class="form-input">
                                    <input class="form-control" data-validation="senha" data-validation-error-msg="Por favor preencha o campo!" id="client_contact-password" name="senha" placeholder="Senha" type="password">
                                </section>
                            </section>
                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg">Logar</button>
                            </section>
                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
    <!--footer-->
   <footer>
            <section class="row">
			    <a href="#"><i class="fa fa-twitter fa-2x"> </i></a>
                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
			 <section> <span class="text"> Rafaela Caroline Storti - 3infoI1 </span> </section>
            </section>
   </footer>
	

    <!-- SCRIPTS-->
    <script src="../interface/bootstrap-3.3.6/js/tests/vendor/jquery.min.js"></script>

    <script src="../interface/bootstrap-3.3.6/dist/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <script >

			//jQuery for page scrolling feature - requires jQuery Easing plugin
			$(function() {
    		$('a.page-scroll').bind('click', function(event) {
       	    var $anchor = $(this);
       		 $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
       		 }, 1500, 'easeInOutExpo');
       		 event.preventDefault();
    		});
			});
    </script>
</body>

</html>
