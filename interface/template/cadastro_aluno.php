<?php
  include_once '../../classe/Projeto.php';
  include_once '../../classe/Turma.php';
  include_once '../../classe/Grupo.php';
?>

<section class="container">
  <section class="row">
    <section class="form-dialog ">
      <section class="col-md-6 col-md-offset-3 form-box">
        <section class="form-top">

          <h3>Turmas</h3>
          <h4>Selecione a turma do aluno:</h4>

        </section>
        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">
            <section class="panel-heading" role="tab" >
              <h4 class="panel-title">

                <?php

                $lista_turmas = array();
                $turmas = new Turma();
                $id_usuario = $_SESSION['login']['id_usuario'];
                $turma = $turmas-> apresentaTurmasDoProfessor($id_usuario);

                foreach ($turma as $turmas) :?>
                  <a href="inicial.php?pos=1&pgs=formulario_cadastro_aluno.php&id_turma=<?= $turmas['id_turma'];?>">
                    <h4 class="turma"> Turma <?= $turmas['desc_turma'];?>
                    </h4>                                  
                  </a>
                  <?php 
                  array_push($lista_turmas, $turmas['id_turma']);
                endforeach ?>

              </section>

              <?php foreach ($lista_turmas as $id_turma) : ?>

                <section <?= $id_turma; ?> >

              <?php endforeach ?>

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>

