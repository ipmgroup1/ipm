
<section class="container">
	<section class="row">
		<section class="form-dialog ">
			<section class="col-md-6 col-md-offset-3 form-box">
				<section class="form-top">

					<h3>Cadastro de projeto</h3>

					<section class="modal-body">
						<form class="signup-form " method="post" action="../../controladores/controlador_cad_projeto.php" enctype="multipart/form-data" role="form">

							<section class="form-group ">
								<section class="form-input">
									<input class="form-control" name="nome_projeto" placeholder="Nome" type="text" >
								</section>
							</section>

							<section class="form-group ">
								<section class="form-input">
									<select name="id_turma" class="form-control">
										<option class="form-control">Turma</option>

										<?php
										include_once "../../classe/Turma.php";
										$turma = new Turma();
										$turmas = $turma-> pesquisaTurmas();
										foreach ($turmas as $turma) {
											?>
											<option class="form-control" value="<?=$turma['id_turma'];?>"><?=$turma['desc_turma'];?>
											</option>
											<?php } ?>

										</select>
									</section>
								</section>

								<section class="form-group ">
									<section class="form-input">
										<select name="id_orientador" class="form-control">
											<option class="form-control">Orientador</option>

											<?php
											include_once "../../classe/Usuario.php";
											$orientador = new Usuario ();
											$orientadores = $orientador-> consultaOrientador();
											foreach ($orientadores as $orientador) {
												?>
												<option class="form-control" value="<?=$orientador['id_usuario'];?>"><?=$orientador['nome'];?>
												</option>
												<?php } ?>

											</select>
										</section>
									</section>

									<section class="form-group ">
										<section class="form-input">
											<select name="id_coorientador" class="form-control">
												<option class="form-control">Coorientador</option>

												<?php
												include_once "../../classe/Usuario.php";
												$coorientador = new Usuario ();
												$coorientadores = $coorientador-> consultaCoorientador();
												foreach ($coorientadores as $coorientador) {
													?>
													<option class="form-control" value="<?=$coorientador['id_usuario'];?>"><?=$coorientador['nome'];?>
													</option>
													<?php } ?>

												</select>
											</section>
										</section>

										<section class="form-group">
											<button type="submit" class="btn btn-block btn-primary btn-lg">Cadastrar
											</button>
										</section>

									</form>
								</section>
							</section>
						</section>
					</section>
				</section>
			</section> 



