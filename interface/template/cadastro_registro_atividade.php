
<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Cadastro de sub-atividade</h3>


                    <form class="signup-form " method="post" action="../../controladores/controlador_cad_registro_atividade.php"  role="form">

                        <input type="hidden" name="id_atividade" value="<?= $_GET['id_atividade'] ?>">
                        <input type="hidden" name="id_projeto" value="<?= $_GET['id_projeto'] ?>">

                        <section class="form-group ">
                            <section class="form-input">
                                <input type="text" name="texto" placeholder="Descrição de sub-atividade">
                            </section>
                        </section>

                        <section class="form-group">
                            <button type="submit" class="btn btn-block btn-primary btn-lg">Cadastrar
                            </button>
                        </section>
                    </form>


                </section>
            </section>
        </section>
    </section>
</section>


