<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">

                    <h3>Cadastro de turmas</h3>

                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_turma.php" enctype="multipart/form-data" role="form">

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="turma" placeholder="Nome da turma" type="text">
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input autocapitalize="off" autocorrect="off" class="form-control" name="ano_inicio" placeholder="ano de inicio" type="text">
                                </section>
                            </section>

                            <?php 
                            $id_professor = $_SESSION['login']['id_usuario'];
                            echo ('<input name="id_professor" type="hidden" value="'.$id_professor.'">');
                            ?>

                                <section class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">Cadastrar</button>
                                </section>
                            </form>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>


