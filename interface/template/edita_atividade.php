<?php

$id_atividade = $_GET['id_atividade'];
echo "<form class='signup-form' method='post' enctype='multipart/form-data' action='../../controladores/controlador_edita_atividade.php?id_atividade=".$id_atividade."' role='form'>";

include_once "../../classe/Atividade.php";

$atividade = new Atividade();
$exibicao = $atividade-> apresentaDescAtividade($id_atividade);

?>

<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">

                    <h3> Edição de atividade</h3>

                    <section class="modal-body">
                        <form class="signup-form">

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="edita_atividade" placeholder='<?=$exibicao["desc_atividade"];?>' type="text">
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="edita_data" value="<?=$exibicao["data_previsao"];?>" type="date">
                                </section>
                            </section>

                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg"> Salvar </button>
                            </section>

                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
</section> 