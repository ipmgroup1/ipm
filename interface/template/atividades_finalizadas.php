<!DOCTYPE html>
	<html>
		<head>
    		<title>PRFC</title>

    			<!-- FONTS -->
			    <link href="../fontes/font_awesome.css" rel="stylesheet">
			    <link href="../fontes/lora.css" rel="stylesheet" type="text/css">
			    <link href="../fontes/montserrat.css" rel="stylesheet" type="text/css">

			    <!-- CSS -->
			    <link href="../bootstrap-3.3.6/dist/css/bootstrap.min.css" rel="stylesheet">
			    <link href="../bootstrap-3.3.6/dist/js/collapse.js" rel="stylesheet">
			    <link href="../css/estilo.css" rel="stylesheet"> 
		</head>
		<body>

			<header class="intro dois">
			    <article class="intro-body principal">
					<div class="container">
						<h3>Atividades</h3>
						<p>Lista de atividades para conclusão do projeto final de curso: </p>

							<table class="table table-striped">
								<thead>
									<th>Atividade</th>
								</thead>

								<tbody>

								<?php
								include_once '../../classe/Atividade.php';

								$atividade  = new Atividade();
								$id_projeto = $_GET['id_projeto'];

								$consultas = $atividade->listaAtividadesFinalizadas($id_projeto);
								
								foreach ($consultas as $atividade) : ?>	

									<td> <?=$atividade['desc_atividade'] ?></td>

								<?php endforeach; ?>

							    </tbody>
							    	<section class="form-group">
							  		</section>
							</table>	
					</div>
			    </article>
			</header>
		</body>
	</html>