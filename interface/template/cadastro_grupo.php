<?php

include_once "../../classe/Usuario.php";
$alunos = Usuario::pesquisaAlunoSemProjetos();

?>

<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Cadastro de grupo</h3>
                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_grupo.php"  role="form">

                            <!--ALUNO 1 -->
                            <section class="form-group ">
                                <section class="form-input">
                                    <select name="id_aluno1" class="form-control">
                                        <?php foreach ($alunos as $aluno): ?>
                                            <option class="form-control" value="<?=$aluno['id_usuario'];?>"><?=$aluno['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </section>
                            </section>

                            <!--ALUNO 2 -->
                            <section class="form-group ">
                                <section class="form-input">
                                    <select name="id_aluno2" class="form-control">
                                        <option value="">selecione um aluno</option>
                                        <?php foreach ($alunos as $aluno): ?>
                                            <option class="form-control" value="<?=$aluno['id_usuario'];?>"><?=$aluno['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </section>
                            </section>

                            <!--ALUNO 3 -->
                            <section class="form-group ">
                                <section class="form-input">
                                    <select name="id_aluno3" class="form-control">
                                        <option value="">selecione um aluno</option>
                                        <?php foreach ($alunos as $aluno): ?>
                                            <option class="form-control" value="<?=$aluno['id_usuario'];?>"><?=$aluno['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <select name="id_projeto" class="form-control">
                                        <?php
                                        include_once "../../classe/Projeto.php";
                                        $projeto = new Projeto();
                                        $projetos = $projeto-> pesquisaProjetos();
                                        foreach ($projetos as $projeto) {
                                            ?>
                                            <option class="form-control" value="<?=$projeto['id_projeto'];?>"><?=$projeto['nome_projeto'];?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </section>
                                </section>

                                <section class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">Cadastre-se</button>
                                </section>
                            </form>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>


