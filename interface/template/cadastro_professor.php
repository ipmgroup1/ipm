<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Cadastro de professor</h3>
                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_usuario.php" enctype="multipart/form-data" role="form">
                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="nome" placeholder="Nome" type="text" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="email" placeholder="email" type="email" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="senha" placeholder="senha" type="password" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="confirmar_senha" placeholder="confirmar senha" type="password" >
                                </section>
                            </section>

                            <input  name="id_tipo" value="2" type="hidden" >

                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg">Cadastrar</button>
                            </section>

                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
</section>

