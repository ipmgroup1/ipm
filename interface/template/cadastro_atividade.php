
<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Projeto 
                        <?php
                        include_once('../../classe/Projeto.php');
                        $projeto = new Projeto(5, 1, 1, 1);
                        $id_projeto = $_GET['id_projeto'];
                        $projetos = $projeto->retornaNomeProjeto($id_projeto);
                        foreach ($projeto as $item) {
                            print($item['nome_projeto']);
                        }
                        ?>
                    </h3>

                    <h3>Cadastro de atividade</h3>
                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_atividade.php"  role="form">

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="desc_atividade" placeholder="Descrição da atividade" type="text" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="id_projeto" value="<?= $id_projeto ?>" placeholder="ID do Projeto" type="hidden">
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <input class="form-control" name="data_previsao" placeholder="Previsão de entrega" type="date" >
                                </section>
                            </section>


                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg">Cadastrar atividade</button>
                            </section>
                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
</section>


