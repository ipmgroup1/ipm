<!DOCTYPE html>
	<html>
		<head>
    		<title>PRFC</title>

    			<!-- FONTS -->
			    <link href="../fontes/font_awesome.css" rel="stylesheet">
			    <link href="../fontes/lora.css" rel="stylesheet" type="text/css">
			    <link href="../fontes/montserrat.css" rel="stylesheet" type="text/css">

			    <!-- CSS -->
			    <link href="../bootstrap-3.3.6/dist/css/bootstrap.min.css" rel="stylesheet">
			    <link href="../bootstrap-3.3.6/dist/js/collapse.js" rel="stylesheet">
			    <link href="../css/estilo.css" rel="stylesheet"> 
		</head>
		<body>

			<header class="intro dois">
			    <article class="intro-body principal">
					<div class="container">
						<h3>Atividades</h3>
						<p>Lista de atividades para conclusão do projeto final de curso: </p>

							<a href="inicial.php?pos=1&id_projeto=<?= $_GET['id_projeto'];?>&pgs=atividades_finalizadas.php"> <button type="submit" class="btn btn-block btn-primary btn-lg atividade"> <span class="glyphicon glyphicon-ok" aria-hidden="true"> </span>Atividades finalizadas</button> </a>

							<?php if ($_SESSION['login']['id_tipo'] < 5): ?>
							  	<a href="inicial.php?pos=1&id_projeto=<?= $_GET['id_projeto'];?>&pgs=cadastro_atividade.php"> <button type="submit" class="btn btn-block btn-primary btn-lg atividade"> <span class="glyphicon glyphicon-plus" aria-hidden="true"> </span> Cadastrar atividade</button> </a>
							<?php endif; ?>

							<table class="table table-striped">
								<thead>
									<tr>
										<?php if ($_SESSION['login']['id_tipo'] < 5) {
										echo "<th></th>"; }
										?>
										<th>Atividade</th>
										<th>Data Prevista</th>
									</tr>
								</thead>
								<tbody>

								<?php
								include_once '../../classe/Atividade.php';

								$atividade  = new Atividade();
								$id_projeto = $_GET['id_projeto'];
								$consultas = $atividade->pesquisaAtividade($id_projeto);
					

								foreach ($consultas as $atividade) : ?>

						<?php if ($_SESSION['login']['id_tipo'] < 5): ?>
							<td> 

								<a href="inicial.php?id_atividade=<?=$atividade['id_atividade'];?>&id_projeto=<?=$_GET['id_projeto'];?>&pgs=edita_atividade.php&pos=1"><span class="glyphicon glyphicon-pencil"></span>Editar</a>

								<a href="../../controladores/controlador_exclui_atividade.php?id_atividade=<?=$atividade['id_atividade'];?>&id_projeto=<?=$_GET['id_projeto'];?>" ><span class="glyphicon glyphicon-trash"></span>Excluir</a>
							</td>
                        <?php endif; ?>
                            			
							<td> <?= $atividade['desc_atividade']; 

								$id_atividade = $atividade['id_atividade']; 
								$registros = new Atividade();
								$registro = $registros->pesquisaRegistroAtividade($id_atividade);

								?>
								
								<?php foreach ($registro as $registros): ?>
									<ul class="linha">
										<li > <?= $registros['texto'] ?> </li>
										<li> <?= $registros['data'] ?> </li>
									</ul>
								<?php endforeach ?>
							</td>

							<td> <?= $atividade['data'] ?></td>

                            <?php if ($_SESSION['login']['id_tipo'] == 5): ?>
                            	<td>
	                            	<a href="../../controladores/controlador_finalizar.php?id_atividade=<?=$atividade['id_atividade'];?>&id_projeto=<?=$_GET['id_projeto'];?>"><span class="glyphicon glyphicon-ok"> Finalizar </a>


	                            	<a href='inicial.php?pos=1&id_atividade=<?=$atividade["id_atividade"];?>&id_projeto=<?=$_GET['id_projeto'];?>&pgs=cadastro_registro_atividade.php'><span class="glyphicon glyphicon-plus">Cadastar sub-atividade</a>
                            	</td>
                            <?php endif; ?>

                        </tr>
                    			<?php endforeach; ?>
                            	
								</tbody>
							    	
							  </table> 	
					</div>
			    </article>
			</header>
		</body>
	</html>