<?php

	session_start();
	include_once "../classe/Atividade.php";
	
	$atividade = new Atividade();
	$texto = filter_input(INPUT_POST, 'texto');
	$data_envio = date("Y-m-d");
	$id_aluno = $_SESSION['login']['id_usuario'];
	$id_atividade = filter_input(INPUT_POST, 'id_atividade');
	$id_projeto = filter_input(INPUT_POST,'id_projeto');
	
	$resultado = $atividade->cadastraRegistroAtividade($texto, $data_envio, $id_aluno, $id_atividade);

	header("location:../interface/template/inicial.php?pos=1&id_atividade=$id_atividade&id_projeto=$id_projeto&pgs=projeto.php");