<?php

	include_once "../classe/Turma.php";

	$desc_turma = filter_input(INPUT_POST, 'turma');;
	$ano_inicio = filter_input(INPUT_POST, 'ano_inicio');
	$id_professor = filter_input(INPUT_POST, 'id_professor');
	
	$turma = new Turma();
	$resultado = $turma->cadastraTurma($desc_turma, $ano_inicio, $id_professor);

		if ($resultado == true) {
			echo ("<script type='text/javascript'> alert('Parabéns, o usuario foi cadastrado com sucesso!'); location.href='../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php'; </script>");
		
		}else{
			echo ("<script type='text/javascript'> alert('O cadastro não pode ser realizado!'); location.href='../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php'; </script>");
		}