<?php

	include_once "../classe/Projeto.php";

	$id_orientador = filter_input(INPUT_POST, 'id_orientador');
	$id_coorientador = filter_input(INPUT_POST, 'id_coorientador');
	$id_turma = filter_input(INPUT_POST, 'id_turma');
	$nome_projeto = filter_input(INPUT_POST, 'nome_projeto');

	$projeto = new Projeto();

	$resultado = $projeto->cadastraProjeto($id_orientador, $id_coorientador,$id_turma, $nome_projeto);

	if($resultado == true) {
		echo ("<script type='text/javascript'> alert('Projeto cadastrado com sucesso!'); location:../interface/template/inicial.php?pos=1&pgs=cadastro_projeto.php'; </script>");
	
	}else{
		echo ("<script type='text/javascript'> alert('Cadastro não pode ser realizado'); location:../interface/template/inicial.php?pos=1&pgs=cadastro_projeto.php'; </script>");
	};
	