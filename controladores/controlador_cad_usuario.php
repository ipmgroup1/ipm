<?php

	include_once "../classe/Usuario.php";

	$nome = filter_input(INPUT_POST, 'nome');
	$email = filter_input(INPUT_POST, 'email');
	$senha = filter_input(INPUT_POST, 'senha');
	$confirmar_senha = filter_input(INPUT_POST, 'confirmar_senha');
	$id_tipo = filter_input(INPUT_POST, 'id_tipo');
	
	if ($senha==$confirmar_senha) {
		$usuario = new Usuario($nome, $email, $senha, $id_tipo);

		$pesquisa = $usuario->conferiUsuario($email);

			if ($pesquisa==false) {
				$resultado = $usuario->cadastraUsuario($nome, $email, $senha, $id_tipo);}

				if($resultado == true) {
				echo ("<script type='text/javascript'> alert('Parabéns, o usuario foi cadastrado com sucesso!'); location.href='../interface/template/inicial.php?pos=1&pgs=inicial.php'; </script>");
		
		}	else{
			echo ("<script type='text/javascript'> alert('O cadastro não pode ser realizado, email já cadastrado'); location.href='../interface/template/inicial.php?pos=1&pgs=inicial.php'; </script>");
		};
	}
	else {
		echo ("<script type='text/javascript'> alert('Senhas diferentes'); location.href='../interface/template/inicial.php?pos=1&pgs=inicial.php'; </script>");
	}