<?php

	include_once "../classe/Grupo.php";

	$id_projeto = filter_input(INPUT_POST, 'id_projeto');
	$id_aluno1 = filter_input(INPUT_POST, 'id_aluno1');
	$id_aluno2 = filter_input(INPUT_POST, 'id_aluno2');
	$id_aluno3 = filter_input(INPUT_POST, 'id_aluno3');	
	
	$grupo = new Grupo();

	$resultado = $grupo->cadastraGrupo($id_projeto, $id_aluno1 ,$id_aluno2 ,$id_aluno3);

	if($resultado) {
		echo ("<script> alert('Grupo cadastrado com sucesso!'); </script>");
	
	}else{
		echo ("<script> alert('O cadastro não pode ser relizado!');</script>");
	}