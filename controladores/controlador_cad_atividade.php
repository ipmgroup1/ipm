<?php

	include_once "../classe/Atividade.php";

	$desc_atividade = filter_input(INPUT_POST, 'desc_atividade');
	$data_previsao = filter_input(INPUT_POST, 'data_previsao');
	$id_projeto = filter_input(INPUT_POST, 'id_projeto');

	$atividade = new Atividade();

	$resultado = $atividade->cadastraAtividade($desc_atividade, $data_previsao, $id_projeto);

	if($resultado == true) {
		echo ("<script type='text/javascript'> alert('Atividade cadastrado com sucesso!'); location:../interface/template/inicial.php?pos=1&pgs=cadastro_atividade.php'; </script>");
	
	}else{
		echo ("<script type='text/javascript'> alert('Cadastro não pode ser realizado'); location:../interface/template/inicial.php?pos=1&pgs=cadastro_atividade.php'; </script>");
	};