<?php

	include_once "../classe/Login.php";

	$email = filter_input(INPUT_POST, 'email');
	$senha =  filter_input(INPUT_POST, 'senha');
	
	$login = new Login();

	$login->efetuaLogin($email, $senha);

    $teste = $login->isLogado();

    if ($teste == true) {
 
	header("location:../interface/template/inicial.php");
 	
    }else{
	    echo ("<script type='text/javascript'> alert('Email ou senha incorreto!'); location:../interface/index.php'; </script>");
	}
    
 ?>